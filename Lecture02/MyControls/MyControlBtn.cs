﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

// The Templated Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234235

namespace Lecture02.MyControls
{
    public sealed class MyControlBtn : Control
    {
        public MyControlBtn()
        {
            this.DefaultStyleKey = typeof(MyControlBtn);
        }



        public String CheckText
        {
            get { return (string)GetValue(CheckTextProperty); }
            set { SetValue(CheckTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CheckText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CheckTextProperty =
            DependencyProperty.Register(nameof(CheckText), typeof(string), typeof(MyControlBtn), new PropertyMetadata("I agree"));


    }
}
